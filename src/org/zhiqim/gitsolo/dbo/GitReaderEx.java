/*
 * 版权所有 (C) 2015 知启蒙(ZHIQIM) 保留所有权利。[遇见知启蒙，邂逅框架梦，本文采用木兰宽松许可证第2版]
 * 
 * https://zhiqim.org/project/zhiqim_products/gitsolo.htm
 *
 * gitsolo is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */
package org.zhiqim.gitsolo.dbo;

import org.zhiqim.gitsolo.dbo.GitReader;

import org.zhiqim.kernel.annotation.AnAlias;
import org.zhiqim.kernel.annotation.AnNew;
import org.zhiqim.kernel.json.Jsons;
import org.zhiqim.orm.annotation.*;

/**
 * 个人汇报视图 对应视图《GIT_READER_EX》
 */
@AnAlias("GitReaderEx")
@AnNew
@AnView("GIT_READER,ZMR_OPERATOR a,ZMR_OPERATOR b")
@AnViewJoin({@AnViewJoinValue(type="EQUAL", lTable="GIT_READER", lColumn="OPERATOR_CODE", rTable="a", rColumn="OPERATOR_CODE"),
             @AnViewJoinValue(type="EQUAL", lTable="GIT_READER", lColumn="READER_CODE", rTable="b", rColumn="OPERATOR_CODE")})
public class GitReaderEx extends GitReader
{
    private static final long serialVersionUID = 1L;

    @AnViewField(table="a", column="OPERATOR_NAME")    private String operatorName;    //2.操作员名称
    @AnViewField(table="a", column="OPERATOR_AVATAR")    private long operatorAvatar;    //3.操作员头像编号
    @AnViewField(table="b", column="OPERATOR_NAME")    private String readerName;    //4.
    @AnViewField(table="b", column="OPERATOR_AVATAR")    private long readerAvatar;    //5.

    public String toString()
    {
        return Jsons.toString(this);
    }

    public String getOperatorName()
    {
        return operatorName;
    }

    public void setOperatorName(String operatorName)
    {
        this.operatorName = operatorName;
    }

    public long getOperatorAvatar()
    {
        return operatorAvatar;
    }

    public void setOperatorAvatar(long operatorAvatar)
    {
        this.operatorAvatar = operatorAvatar;
    }

    public String getReaderName()
    {
        return readerName;
    }

    public void setReaderName(String readerName)
    {
        this.readerName = readerName;
    }

    public long getReaderAvatar()
    {
        return readerAvatar;
    }

    public void setReaderAvatar(long readerAvatar)
    {
        this.readerAvatar = readerAvatar;
    }

}
