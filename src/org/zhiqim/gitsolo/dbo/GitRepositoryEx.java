/*
 * 版权所有 (C) 2015 知启蒙(ZHIQIM) 保留所有权利。[遇见知启蒙，邂逅框架梦，本文采用木兰宽松许可证第2版]
 * 
 * https://zhiqim.org/project/zhiqim_products/gitsolo.htm
 *
 * gitsolo is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */
package org.zhiqim.gitsolo.dbo;

import org.zhiqim.gitsolo.dbo.GitRepository;

import org.zhiqim.kernel.annotation.AnAlias;
import org.zhiqim.kernel.annotation.AnNew;
import org.zhiqim.kernel.json.Jsons;
import org.zhiqim.orm.annotation.*;

/**
 * 项目代码仓库视图 对应视图《GIT_REPOSITORY_EX》
 */
@AnAlias("GitRepositoryEx")
@AnNew
@AnView("GIT_REPOSITORY,GIT_PROJECT")
@AnViewJoin({@AnViewJoinValue(type="EQUAL", lTable="GIT_REPOSITORY", lColumn="PROJECT_ID", rTable="GIT_PROJECT", rColumn="PROJECT_ID")})
public class GitRepositoryEx extends GitRepository
{
    private static final long serialVersionUID = 1L;

    @AnViewField(table="GIT_PROJECT", column="PROJECT_MANAGER")    private String projectManager;    //2.项目组长

    public String toString()
    {
        return Jsons.toString(this);
    }

    public String getProjectManager()
    {
        return projectManager;
    }

    public void setProjectManager(String projectManager)
    {
        this.projectManager = projectManager;
    }

}
