/*
 * 版权所有 (C) 2015 知启蒙(ZHIQIM) 保留所有权利。[遇见知启蒙，邂逅框架梦，本文采用木兰宽松许可证第2版]
 * 
 * https://zhiqim.org/project/zhiqim_products/gitsolo.htm
 *
 * gitsolo is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */
package org.zhiqim.gitsolo.dbo;

import java.io.Serializable;
import java.sql.Timestamp;

import org.zhiqim.kernel.annotation.AnAlias;
import org.zhiqim.kernel.annotation.AnNew;
import org.zhiqim.kernel.json.Jsons;
import org.zhiqim.orm.annotation.*;

/**
 * 项目代码仓库表 对应表《GIT_REPOSITORY》
 */
@AnAlias("GitRepository")
@AnNew
@AnTable(table="GIT_REPOSITORY", key="REPOSITORY_ID", type="InnoDB")
public class GitRepository implements Serializable
{
    private static final long serialVersionUID = 1L;

    @AnTableField(column="PROJECT_ID", type="long", notNull=true)    private long projectId;    //1.项目编号
    @AnTableField(column="REPOSITORY_ID", type="long", notNull=true)    private long repositoryId;    //2.仓库编号
    @AnTableField(column="REPOSITORY_CODE", type="string,32", notNull=true)    private String repositoryCode;    //3.仓库编码
    @AnTableField(column="REPOSITORY_NAME", type="string,64", notNull=true)    private String repositoryName;    //4.仓库名称
    @AnTableField(column="REPOSITORY_STATUS", type="byte", notNull=true)    private int repositoryStatus;    //5.仓库状态，0：正常，1：停用
    @AnTableField(column="REPOSITORY_SEQ", type="int", notNull=true)    private int repositorySeq;    //6.仓库排序，整型，从小到大
    @AnTableField(column="REPOSITORY_UPDATE_ROLE", type="string,64", notNull=false)    private String repositoryUpdateRole;    //7.仓库角色，为空表示全支持，不为空多个角色用逗号隔开，如manage,test
    @AnTableField(column="REPOSITORY_COMMIT_ROLE", type="string,64", notNull=false)    private String repositoryCommitRole;    //8.仓库角色，为空表示全支持，不为空多个角色用逗号隔开，如manage,test
    @AnTableField(column="REPOSITORY_CREATOR", type="string,32", notNull=true)    private String repositoryCreator;    //9.仓库创建者
    @AnTableField(column="REPOSITORY_CREATED", type="datetime", notNull=true)    private Timestamp repositoryCreated;    //10.仓库创建时间
    @AnTableField(column="REPOSITORY_MODIFIED", type="datetime", notNull=true)    private Timestamp repositoryModified;    //11.仓库更新时间

    public String toString()
    {
        return Jsons.toString(this);
    }

    public long getProjectId()
    {
        return projectId;
    }

    public void setProjectId(long projectId)
    {
        this.projectId = projectId;
    }

    public long getRepositoryId()
    {
        return repositoryId;
    }

    public void setRepositoryId(long repositoryId)
    {
        this.repositoryId = repositoryId;
    }

    public String getRepositoryCode()
    {
        return repositoryCode;
    }

    public void setRepositoryCode(String repositoryCode)
    {
        this.repositoryCode = repositoryCode;
    }

    public String getRepositoryName()
    {
        return repositoryName;
    }

    public void setRepositoryName(String repositoryName)
    {
        this.repositoryName = repositoryName;
    }

    public int getRepositoryStatus()
    {
        return repositoryStatus;
    }

    public void setRepositoryStatus(int repositoryStatus)
    {
        this.repositoryStatus = repositoryStatus;
    }

    public int getRepositorySeq()
    {
        return repositorySeq;
    }

    public void setRepositorySeq(int repositorySeq)
    {
        this.repositorySeq = repositorySeq;
    }

    public String getRepositoryUpdateRole()
    {
        return repositoryUpdateRole;
    }

    public void setRepositoryUpdateRole(String repositoryUpdateRole)
    {
        this.repositoryUpdateRole = repositoryUpdateRole;
    }

    public String getRepositoryCommitRole()
    {
        return repositoryCommitRole;
    }

    public void setRepositoryCommitRole(String repositoryCommitRole)
    {
        this.repositoryCommitRole = repositoryCommitRole;
    }

    public String getRepositoryCreator()
    {
        return repositoryCreator;
    }

    public void setRepositoryCreator(String repositoryCreator)
    {
        this.repositoryCreator = repositoryCreator;
    }

    public Timestamp getRepositoryCreated()
    {
        return repositoryCreated;
    }

    public void setRepositoryCreated(Timestamp repositoryCreated)
    {
        this.repositoryCreated = repositoryCreated;
    }

    public Timestamp getRepositoryModified()
    {
        return repositoryModified;
    }

    public void setRepositoryModified(Timestamp repositoryModified)
    {
        this.repositoryModified = repositoryModified;
    }

}
