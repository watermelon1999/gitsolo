/*
 * 版权所有 (C) 2015 知启蒙(ZHIQIM) 保留所有权利。[遇见知启蒙，邂逅框架梦，本文采用木兰宽松许可证第2版]
 * 
 * https://zhiqim.org/project/zhiqim_products/gitsolo.htm
 *
 * gitsolo is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */
package org.zhiqim.gitsolo.dbo;

import org.zhiqim.gitsolo.dbo.GitMember;

import org.zhiqim.kernel.annotation.AnAlias;
import org.zhiqim.kernel.annotation.AnNew;
import org.zhiqim.kernel.json.Jsons;
import org.zhiqim.orm.annotation.*;

/**
 * 项目成员视图 对应视图《GIT_MEMBER_EX》
 */
@AnAlias("GitMemberEx")
@AnNew
@AnView("GIT_MEMBER,ZMR_OPERATOR")
@AnViewJoin({@AnViewJoinValue(type="EQUAL", lTable="GIT_MEMBER", lColumn="OPERATOR_CODE", rTable="ZMR_OPERATOR", rColumn="OPERATOR_CODE")})
public class GitMemberEx extends GitMember
{
    private static final long serialVersionUID = 1L;

    @AnViewField(table="ZMR_OPERATOR", column="OPERATOR_TYPE")    private int operatorType;    //2.操作员类型：0：超级管理员,1：管理员，2:操作员
    @AnViewField(table="ZMR_OPERATOR", column="OPERATOR_NAME")    private String operatorName;    //3.操作员名称
    @AnViewField(table="ZMR_OPERATOR", column="OPERATOR_AVATAR")    private long operatorAvatar;    //4.操作员头像编号

    public String toString()
    {
        return Jsons.toString(this);
    }

    public int getOperatorType()
    {
        return operatorType;
    }

    public void setOperatorType(int operatorType)
    {
        this.operatorType = operatorType;
    }

    public String getOperatorName()
    {
        return operatorName;
    }

    public void setOperatorName(String operatorName)
    {
        this.operatorName = operatorName;
    }

    public long getOperatorAvatar()
    {
        return operatorAvatar;
    }

    public void setOperatorAvatar(long operatorAvatar)
    {
        this.operatorAvatar = operatorAvatar;
    }

}
