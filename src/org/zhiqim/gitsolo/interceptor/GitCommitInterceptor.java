/*
 * 版权所有 (C) 2015 知启蒙(ZHIQIM) 保留所有权利。[遇见知启蒙，邂逅框架梦，本文采用木兰宽松许可证第2版]
 * 
 * https://zhiqim.org/project/zhiqim_products/gitsolo.htm
 *
 * gitsolo is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */
package org.zhiqim.gitsolo.interceptor;

import org.zhiqim.git.GitConstants;
import org.zhiqim.httpd.HttpRequest;
import org.zhiqim.httpd.context.core.Interceptor;
import org.zhiqim.kernel.annotation.AnAlias;
import org.zhiqim.kernel.constants.ZhiqimConstants;

/**
 * 检查仓库是否存在拦截器
 *
 * @version v1.0.0 @author zouzhigang 2016-10-24 新建与整理
 */
@AnAlias("chkGitCommit")
public class GitCommitInterceptor implements Interceptor, ZhiqimConstants, GitConstants
{
    @Override
    public void intercept(HttpRequest request) throws Exception
    {
        GitUpdateInterceptor.intercept(request, false);
    }
}
