/*
 * 版权所有 (C) 2015 知启蒙(ZHIQIM) 保留所有权利。[遇见知启蒙，邂逅框架梦，本文采用木兰宽松许可证第2版]
 * 
 * https://zhiqim.org/project/zhiqim_products/gitsolo.htm
 *
 * gitsolo is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */
package org.zhiqim.gitsolo.rule;

import org.zhiqim.gitsolo.GitsoloConstants;
import org.zhiqim.gitsolo.dao.GitProjectDao;
import org.zhiqim.gitsolo.dbo.GitMember;
import org.zhiqim.httpd.HttpRequest;
import org.zhiqim.httpd.context.rule.CheckRule;
import org.zhiqim.kernel.annotation.AnAlias;
import org.zhiqim.kernel.annotation.AnGlobal;
import org.zhiqim.manager.ZmrSessionUser;

/**
 * 页面验证是否有项目负责人权限，返回boolean =true表示有，=false表示没有
 *
 * @version v1.0.0 @author zouzhigang 2017-10-16 新建与整理
 */
@AnAlias("GitAdminRule")
@AnGlobal
public class GitAdminRule implements CheckRule, GitsoloConstants
{
    public boolean check(HttpRequest request) throws Exception
    {
        ZmrSessionUser sessionUser = request.getSessionUser(ZmrSessionUser.class);
        if (sessionUser == null)
        {// 用户未登录或超时
            return false;
        }
        
        if (sessionUser.isAdmin())
        {//管理员或超级管理员有所有项目权限
            return true;
        }
        
        long projectId = sessionUser.getOperatorParamLong(GIT_PROJECT_ID_KEY);
        if (projectId == -1)
        {//未选中项目
            return false;
        }
        
        GitMember mem = (GitMember)sessionUser.getValue(GIT_MEMBER_KEY);
        if (mem == null || mem.getProjectId() != projectId)
        {//有选中项目，但未加载项目对象到会话中时，主动加载一次
            GitProjectDao.setProjectMember(request, sessionUser, projectId);
            mem = (GitMember)sessionUser.getValue(GIT_MEMBER_KEY);
        }
        
        return mem != null && mem.getMemberType() == 0;
    }
}
