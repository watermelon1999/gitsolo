/*
 * 版权所有 (C) 2015 知启蒙(ZHIQIM) 保留所有权利。[遇见知启蒙，邂逅框架梦，本文采用木兰宽松许可证第2版]
 * 
 * https://zhiqim.org/project/zhiqim_products/gitsolo.htm
 *
 * gitsolo is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */
package org.zhiqim.gitsolo;

import java.io.IOException;
import java.io.OutputStream;

import org.zhiqim.git.constants.GitNameConstants;
import org.zhiqim.git.http.HttpHandler;
import org.zhiqim.git.util.GitStreams;
import org.zhiqim.httpd.HttpResponse;
import org.zhiqim.kernel.constants.HttpConstants;
import org.zhiqim.kernel.util.Longs;
import org.zhiqim.kernel.util.Strings;
import org.zhiqim.kernel.util.consts.Lng;

/**
 * Gitsolo工具类
 *
 * @version v1.0.0 @author zouzhigang 2017-11-27 新建与整理
 */
public class Gitsolo implements GitNameConstants, GitsoloConstants, HttpConstants
{
    /**
     * 发送错误信息
     * 
     * @param response      HTTP响应
     * @param pack          上传或接受包
     * @param error         错误信息
     * @throws IOException  IO异常
     */
    public static void sendError(HttpResponse response, HttpHandler pack, String error) throws IOException
    {
        OutputStream out = response.getOutputStream();

        if (pack.hasSideBand())
            GitStreams.writeSideBandError(out, "error: "+error);
        else
            GitStreams.writeString(out, "error: "+error);
        
        response.setContentType(pack.getContentType());
        response.commit();
    }
    
    /**
     * 发送需要注验证信息的响应
     * 
     * @param response
     * @throws IOException
     */
    public static void sendUnauthorized(HttpResponse response) throws IOException
    {
        response.setHeader(_WWW_AUTHENTICATE_, "Basic realm=\"gitsolo\"");
        response.sendError(_401_UNAUTHORIZED_);
    }
    
    /**
     * 通过类型、仓库编码获取仓库路径，得到如/1710131047430003/gitsolo.git的名称
     * 
     * @param projectId         项目编号
     * @param repositoryCode    仓库编码
     * @return
     */
    public static String getRepositoryPath(long projectId, String repositoryCode)
    {
        return "/" + projectId + "/" + repositoryCode + ".git";
    }
    

    /**
     * 通过访问路径获取仓库名称，得到如/1710131047430003/gitsolo.git的名称
     * 
     * @param pathInContext    上下文中的路径/gitsolo/1710131047430003/ZhiqimGit.git/git-upload-pack
     * @return                 仓库名称如/1710131047430003/ZhiqimGit.git
     */
    public static String getRepositoryName(String pathInContext)
    {
        int ind = pathInContext.indexOf(_GIT);
        if (ind == -1)
            return null;
        
        return Strings.trimLeft(pathInContext.substring(0, ind+4), GIT_URL_PREFIX);
    }
    
    /**
     * 通过仓库名称/1710131047430003/gitsolo.git，得到{1710131047430003, gitsolo}
     * 
     * @param repositoryName    仓库名称/organization/gitsolo.git
     * @return                  Lng对象{1710131047430003, gitsolo}
     */
    public static Lng getRepositoryCode(String repositoryName)
    {
        repositoryName = Strings.trim(repositoryName, "/", _GIT);
        String[] strs = repositoryName.split("/");
        
        return new Lng(Longs.toLong(strs[0]), strs[1]);
    }
}
