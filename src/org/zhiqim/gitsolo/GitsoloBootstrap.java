/*
 * 版权所有 (C) 2015 知启蒙(ZHIQIM) 保留所有权利。[遇见知启蒙，邂逅框架梦，本文采用木兰宽松许可证第2版]
 * 
 * https://zhiqim.org/project/zhiqim_products/gitsolo.htm
 *
 * gitsolo is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */
package org.zhiqim.gitsolo;

import java.sql.Timestamp;

import org.zhiqim.gitsolo.dbo.GitMember;
import org.zhiqim.gitsolo.dbo.GitProject;
import org.zhiqim.gitsolo.dbo.GitReport;
import org.zhiqim.httpd.context.ZmlBootstrap;
import org.zhiqim.kernel.util.DateTimes;
import org.zhiqim.kernel.util.Ids;
import org.zhiqim.kernel.util.Sqls;
import org.zhiqim.manager.ZmrConstants;
import org.zhiqim.manager.dbo.ZmrParamOperator;
import org.zhiqim.orm.ORM;

/**
 * Gitsolo初始化引导类
 *
 * @version v1.0.0 @author zouzhigang 2021-4-2 新建与整理
 */
public class GitsoloBootstrap extends ZmlBootstrap implements GitsoloConstants, ZmrConstants
{
    @Override /** 在配置加载之后,在ZML配置之后 */
    protected void initAfter() throws Exception
    {
        //强制使用03_elegant主题
        context.setAttribute(ZMR_THEME_MAIN, ZMR_THEME_03_ELEGANT);
        
        initProject();
        initParamOperator();
    }
    
    /** 初始化一个工程 */
    private void initProject() throws Exception
    {
        int count = ORM.table().count(GitProject.class);
        if (count > 0)
            return;
        
        String date = DateTimes.getDateString();
        Timestamp datetime = Sqls.nowTimestamp();
        
        //工程
        GitProject project = new GitProject();
        project.setProjectId(Ids.longId13());
        project.setProjectSeq(100);
        project.setProjectBeginDate(date);
        project.setProjectEndDate(DateTimes.getNextDateStringByMonth(12));
        project.setProjectManager("zhiqim");
        project.setProjectCreated(datetime);
        project.setProjectModified(datetime);
        project.setProjectStatus(0);
        project.setProjectName("演示项目");
        project.setProjectDesc("演示项目，可修改或删除重建");
        ORM.table().insert(project);
        
        //成员
        GitMember member = new GitMember();
        member.setProjectId(project.getProjectId());
        member.setOperatorCode("zhiqim");
        member.setMemberType(0);
        ORM.table().insert(member);
        
        //动态
        GitReport report = new GitReport();
        report.setReportId(Ids.longId());
        report.setProjectId(project.getProjectId());
        report.setOperatorCode("zhiqim");
        report.setReportTime(datetime);
        report.setReportDesc("创建了项目 $演示项目");
        ORM.table().insert(report);
    }
    
    /** 初始化操作员参数 */
    private void initParamOperator() throws Exception
    {
        if (ORM.table().count(ZmrParamOperator.class, GIT_SECRET_KEY) == 0)
        {
            ZmrParamOperator item = new ZmrParamOperator();
            item.setParamKey(GIT_SECRET_KEY);
            item.setParamName("Git独立密钥");
            item.setParamSeq(100);
            item.setParamType("private");
            item.setParamDesc("请在Git独立密钥功能中查看和修改");
            
            ORM.table().insert(item);
        }
    }
}
