/*
 * 版权所有 (C) 2015 知启蒙(ZHIQIM) 保留所有权利。[遇见知启蒙，邂逅框架梦，本文采用木兰宽松许可证第2版]
 * 
 * https://zhiqim.org/project/zhiqim_products/gitsolo.htm
 *
 * gitsolo is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */
package org.zhiqim.gitsolo.model;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import org.zhiqim.git.objer.model.PersonIdent;
import org.zhiqim.git.objer.objectid.RevCommit;
import org.zhiqim.kernel.util.Strings;

/**
 * 提交日志模型，对RevCommit进行封装解析
 *
 * @version v1.0.0 @author zouzhigang 2016-11-1 新建与整理
 */
public class CommitModel implements Serializable, Comparable<CommitModel>
{
    private static final long serialVersionUID = 1L;

    public final String repository;
    public final String branch;
    private final RevCommit commit;

    public CommitModel(String repository, String branch, RevCommit commit)
    {
        this.repository = repository;
        this.branch = branch;
        this.commit = commit;
    }

    public RevCommit getCommit()
    {
        return commit;
    }

    public String getName()
    {
        return commit.getName();
    }

    public String getShortName()
    {
        return commit.getName().substring(0, 8);
    }

    public String getShortMessage()
    {
        return commit.getShortMessage();
    }

    public Date getCommitDate()
    {
        return new Date(commit.getCommitTime() * 1000L);
    }

    public int getParentCount()
    {
        return commit.getParentCount();
    }

    public List<RevCommit> getParents()
    {
        return commit.getParents();
    }

    public PersonIdent getAuthorIdent()
    {
        return commit.getAuthorIdent();
    }

    public PersonIdent getCommitterIdent()
    {
        return commit.getCommitterIdent();
    }

    @Override
    public boolean equals(Object o)
    {
        if (o instanceof CommitModel)
        {
            CommitModel commit = (CommitModel) o;
            return repository.equals(commit.repository) && getName().equals(commit.getName());
        }
        return false;
    }

    @Override
    public int hashCode()
    {
        return (repository + commit).hashCode();
    }

    @Override
    public int compareTo(CommitModel o)
    {
        if (commit.getCommitTime() > o.commit.getCommitTime())
            return -1;
        else if (commit.getCommitTime() < o.commit.getCommitTime())
            return 1;
        else
            return 0;
    }

    @Override
    public String toString()
    {
        return Strings.formatMessage("{0} {1} {2,date,yyyy-MM-dd HH:mm} {3} {4}", 
            getShortName(), branch, 
            getCommitterIdent().getWhen(), 
            getAuthorIdent().getName(), 
            getShortMessage());
    }
}