/*
 * 版权所有 (C) 2015 知启蒙(ZHIQIM) 保留所有权利。[遇见知启蒙，邂逅框架梦，本文采用木兰宽松许可证第2版]
 * 
 * https://zhiqim.org/project/zhiqim_products/gitsolo.htm
 *
 * gitsolo is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */
package org.zhiqim.gitsolo.action;

import org.zhiqim.gitsolo.GitsoloConstants;
import org.zhiqim.gitsolo.dao.GitProjectDao;
import org.zhiqim.gitsolo.dbo.GitMemberEx;
import org.zhiqim.gitsolo.dbo.GitProject;
import org.zhiqim.httpd.HttpRequest;
import org.zhiqim.httpd.context.core.Action;
import org.zhiqim.kernel.paging.PageResult;
import org.zhiqim.orm.ORM;
import org.zhiqim.orm.ZTable;
import org.zhiqim.orm.ZView;
import org.zhiqim.orm.dbo.Selector;

/**
 * 项目成员列表
 *
 * @version v1.0.0 @author huangsufang 2017-9-25 新建与整理
 */
public class MemberAction implements Action, GitsoloConstants
{
    @Override
    public void execute(HttpRequest request) throws Exception
    {
        long projectId = GitProjectDao.getProjectId(request);
        GitProject project = ORM.get(ZTable.class, request).item(GitProject.class, projectId);
            
        int page = request.getParameterInt("page", 1);
        int pageSize = request.getContextAttributeInt("zmr_page_size", 10);
        
        PageResult<GitMemberEx> result = ORM.get(ZView.class, request).page(GitMemberEx.class, page, pageSize, new Selector("projectId", projectId).addOrderbyAsc("memberType"));
        result.addConditionMap(request.getParameterMap());
        
        request.setAttribute("project", project);
        request.setAttribute("result", result);
    }
}
