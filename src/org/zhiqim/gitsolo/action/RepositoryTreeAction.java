/*
 * 版权所有 (C) 2015 知启蒙(ZHIQIM) 保留所有权利。[遇见知启蒙，邂逅框架梦，本文采用木兰宽松许可证第2版]
 * 
 * https://zhiqim.org/project/zhiqim_products/gitsolo.htm
 *
 * gitsolo is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */
package org.zhiqim.gitsolo.action;

import java.util.ArrayList;

import org.zhiqim.git.Git;
import org.zhiqim.git.GitConstants;
import org.zhiqim.git.objer.model.Tree;
import org.zhiqim.git.objer.objectid.RevCommit;
import org.zhiqim.httpd.HttpRequest;
import org.zhiqim.httpd.context.core.Action;
import org.zhiqim.kernel.util.Strings;
import org.zhiqim.kernel.util.Validates;


/**
 * 查看仓库文件列表
 *
 * @version v1.0.0 @author zouzhigang 2016-11-1 新建与整理
 */
public class RepositoryTreeAction implements Action, GitConstants
{
    @Override
    public void execute(HttpRequest request) throws Exception
    {
        Git git = (Git)request.getAttribute(GIT_ATTRIBUTE_REPOSITORY);
        
        String folder = request.getParameter("folder");
        
        if (Validates.isNotEmpty(folder))
        {//目录不为空，增加返回上一级目录链接
            if (folder.indexOf("/") == -1)
                request.setAttribute("lastFolder", "");
            else
                request.setAttribute("lastFolder", Strings.removeRightByLast(folder, "/"));
        }
        
        //读取分枝最后一个提交
        String head = request.getParameter("head", "master");
        RevCommit headCommit = git.resolve(head, RevCommit.class);
        
        if (headCommit == null)
        {//无提交不需要查找文件
            request.setAttribute("tree", new ArrayList<>());
            return;
        }
        
        Tree tree = git.resolveTreeByCommit(headCommit, folder);
        request.setAttribute("tree", tree);
    }
}
