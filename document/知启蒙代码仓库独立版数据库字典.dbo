<?xml version="1.0" encoding="UTF-8"?>
<zhiqim-dbo>

<config version="1.2.0" dbType="mysql" folder="../src" initDataWidth="820">
<![CDATA[
/*
 * 版权所有 (C) 2015 知启蒙(ZHIQIM) 保留所有权利。[遇见知启蒙，邂逅框架梦，本文采用木兰宽松许可证第2版]
 * 
 * https://zhiqim.org/project/zhiqim_products/gitsolo.htm
 *
 * gitsolo is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */
]]>
</config>

<package name="org.zhiqim.manager.dbo" type="false" desc="管理台表">

    <table name="ZMR_OPERATOR" desc="操作员表" key="OPERATOR_CODE" type="InnoDB">
        <field column="OPERATOR_CODE" type="string" length="32" notNull="true" desc="操作员编码"/>
        <field column="OPERATOR_TYPE" type="byte" length="" notNull="true" desc="操作员类型：0：超级管理员,1：管理员，2:操作员"/>
        <field column="OPERATOR_NAME" type="string" length="64" notNull="true" desc="操作员名称"/>
        <field column="OPERATOR_AVATAR" type="long" length="" notNull="false" desc="操作员头像编号"/>
    </table>

</package>
<package name="org.zhiqim.gitsolo.dbo" type="true" desc="项目代码管理">

    <table name="GIT_PROJECT" desc="项目信息表" key="PROJECT_ID" type="InnoDB">
        <field column="PROJECT_ID" type="long" length="" notNull="true" desc="项目编号"/>
        <field column="PROJECT_NAME" type="string" length="64" notNull="true" desc="项目名称"/>
        <field column="PROJECT_STATUS" type="byte" length="" notNull="true" desc="项目状态，0：正常，1：停用"/>
        <field column="PROJECT_SEQ" type="int" length="" notNull="true" desc="项目排序数"/>
        <field column="PROJECT_MANAGER" type="string" length="32" notNull="true" desc="项目组长"/>
        <field column="PROJECT_BEGIN_DATE" type="string" length="10,char" notNull="true" desc="项目开始时间"/>
        <field column="PROJECT_END_DATE" type="string" length="10,char" notNull="true" desc="项目结束时间"/>
        <field column="PROJECT_CREATED" type="datetime" length="" notNull="true" desc="项目创建时间"/>
        <field column="PROJECT_MODIFIED" type="datetime" length="" notNull="true" desc="项目修改时间"/>
        <field column="PROJECT_DESC" type="string" length="512" notNull="false" desc="项目描述"/>
    </table>

    <table name="GIT_MEMBER" desc="项目成员表" key="PROJECT_ID,OPERATOR_CODE" type="InnoDB">
        <field column="PROJECT_ID" type="long" length="" notNull="true" desc="项目编号"/>
        <field column="OPERATOR_CODE" type="string" length="32" notNull="true" desc="操作员编码"/>
        <field column="MEMBER_TYPE" type="byte" length="" notNull="true" desc="成员类型，0：负责人，1：普通成员"/>
        <field column="MEMBER_ROLE" type="string" length="64" notNull="false" desc="成员角色，多个用逗号隔开，如demand,development表示需求和开发角色"/>
    </table>

    <table name="GIT_REPORT" desc="项目动态表" key="REPORT_ID" type="InnoDB">
        <field column="PROJECT_ID" type="long" length="" notNull="true" desc="项目编号"/>
        <field column="OPERATOR_CODE" type="string" length="32" notNull="true" desc="操作员编码"/>
        <field column="REPORT_ID" type="long" length="" notNull="true" desc="动态编号"/>
        <field column="REPORT_TIME" type="datetime" length="" notNull="true" desc="动态时间"/>
        <field column="REPORT_DESC" type="string" length="200" notNull="false" desc="动态描述"/>
    </table>

    <table name="GIT_READER" desc="个人汇报表" key="OPERATOR_CODE,READER_CODE" type="InnoDB">
        <field column="OPERATOR_CODE" type="string" length="32" notNull="true" desc="操作员编码"/>
        <field column="READER_CODE" type="string" length="32" notNull="true" desc="读者编码"/>
    </table>

    <table name="GIT_REPOSITORY" desc="项目代码仓库表" key="REPOSITORY_ID" type="InnoDB">
        <field column="PROJECT_ID" type="long" length="" notNull="true" desc="项目编号"/>
        <field column="REPOSITORY_ID" type="long" length="" notNull="true" desc="仓库编号"/>
        <field column="REPOSITORY_CODE" type="string" length="32" notNull="true" desc="仓库编码"/>
        <field column="REPOSITORY_NAME" type="string" length="64" notNull="true" desc="仓库名称"/>
        <field column="REPOSITORY_STATUS" type="byte" length="" notNull="true" desc="仓库状态，0：正常，1：停用"/>
        <field column="REPOSITORY_SEQ" type="int" length="" notNull="true" desc="仓库排序，整型，从小到大"/>
        <field column="REPOSITORY_UPDATE_ROLE" type="string" length="64" notNull="false" desc="仓库角色，为空表示全支持，不为空多个角色用逗号隔开，如manage,test"/>
        <field column="REPOSITORY_COMMIT_ROLE" type="string" length="64" notNull="false" desc="仓库角色，为空表示全支持，不为空多个角色用逗号隔开，如manage,test"/>
        <field column="REPOSITORY_CREATOR" type="string" length="32" notNull="true" desc="仓库创建者"/>
        <field column="REPOSITORY_CREATED" type="datetime" length="" notNull="true" desc="仓库创建时间"/>
        <field column="REPOSITORY_MODIFIED" type="datetime" length="" notNull="true" desc="仓库更新时间"/>
    </table>

    <view name="GIT_MEMBER_EX" type="true" desc="项目成员视图" table="GIT_MEMBER,ZMR_OPERATOR">
        <join type="EQUAL" lTable="GIT_MEMBER" lColumn="OPERATOR_CODE" rTable="ZMR_OPERATOR" rColumn="OPERATOR_CODE"/>

        <field column="*" table="GIT_MEMBER"/>
        <field column="OPERATOR_TYPE" table="ZMR_OPERATOR"/>
        <field column="OPERATOR_NAME" table="ZMR_OPERATOR"/>
        <field column="OPERATOR_AVATAR" table="ZMR_OPERATOR"/>
    </view>

    <view name="GIT_READER_EX" type="true" desc="个人汇报视图" table="GIT_READER,ZMR_OPERATOR a,ZMR_OPERATOR b">
        <join type="EQUAL" lTable="GIT_READER" lColumn="OPERATOR_CODE" rTable="a" rColumn="OPERATOR_CODE"/>
        <join type="EQUAL" lTable="GIT_READER" lColumn="READER_CODE" rTable="b" rColumn="OPERATOR_CODE"/>

        <field column="*" table="GIT_READER"/>
        <field column="OPERATOR_NAME" table="a"/>
        <field column="OPERATOR_AVATAR" table="a"/>
        <field column="OPERATOR_NAME READER_NAME" table="b"/>
        <field column="OPERATOR_AVATAR READER_AVATAR" table="b"/>
    </view>

    <view name="GIT_REPOSITORY_EX" type="true" desc="项目代码仓库视图" table="GIT_REPOSITORY,GIT_PROJECT">
        <join type="EQUAL" lTable="GIT_REPOSITORY" lColumn="PROJECT_ID" rTable="GIT_PROJECT" rColumn="PROJECT_ID"/>

        <field column="*" table="GIT_REPOSITORY"/>
        <field column="PROJECT_MANAGER" table="GIT_PROJECT"/>
    </view>

    <view name="GIT_MEMBER_PROJECT" type="true" desc="项目成员所有工程视图" table="GIT_MEMBER,GIT_PROJECT">
        <join type="EQUAL" lTable="GIT_MEMBER" lColumn="PROJECT_ID" rTable="GIT_PROJECT" rColumn="PROJECT_ID"/>

        <field column="*" table="GIT_PROJECT"/>
        <field column="OPERATOR_CODE" table="GIT_MEMBER"/>
    </view>

</package>

</zhiqim-dbo>