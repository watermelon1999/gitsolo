-- ---------------------------------------------------
-- 知启蒙代码仓库独立版功能菜单
-- ---------------------------------------------------


truncate table ZMR_MENU;
insert into ZMR_MENU (PARENT_CODE, MENU_CODE, MENU_NAME, MENU_NAME_ABBR, MENU_LEVEL, MENU_STATUS, MENU_TYPE, MENU_ICON, MENU_URL, MENU_DESC) values ('ROOT', 'MENU', '系统功能根菜单', '', 0, 0, 0, '', '', '');
insert into ZMR_MENU (PARENT_CODE, MENU_CODE, MENU_NAME, MENU_NAME_ABBR, MENU_LEVEL, MENU_STATUS, MENU_TYPE, MENU_ICON, MENU_URL, MENU_DESC) values ('MENU', 'MENU_010', '系统配置', '系统', 1, 0, 1, 'z-config', '', '');
insert into ZMR_MENU (PARENT_CODE, MENU_CODE, MENU_NAME, MENU_NAME_ABBR, MENU_LEVEL, MENU_STATUS, MENU_TYPE, MENU_ICON, MENU_URL, MENU_DESC) values ('MENU_010', 'MENU_010_110', '操作员管理', '', 2, 0, 1, 'z-customer', '/${zhiqim_manager}/operator.htm', '');
insert into ZMR_MENU (PARENT_CODE, MENU_CODE, MENU_NAME, MENU_NAME_ABBR, MENU_LEVEL, MENU_STATUS, MENU_TYPE, MENU_ICON, MENU_URL, MENU_DESC) values ('MENU_010_110', 'MENU_010_110_010', '增加操作员', '', 3, 0, 2, '', '/${zhiqim_manager}/operatorAdd.htm,/${zhiqim_manager}/operatorInsert.htm', '');
insert into ZMR_MENU (PARENT_CODE, MENU_CODE, MENU_NAME, MENU_NAME_ABBR, MENU_LEVEL, MENU_STATUS, MENU_TYPE, MENU_ICON, MENU_URL, MENU_DESC) values ('MENU_010_110', 'MENU_010_110_020', '修改操作员', '', 3, 0, 2, '', '/${zhiqim_manager}/operatorModify.htm,/${zhiqim_manager}/operatorUpdate.htm', '');
insert into ZMR_MENU (PARENT_CODE, MENU_CODE, MENU_NAME, MENU_NAME_ABBR, MENU_LEVEL, MENU_STATUS, MENU_TYPE, MENU_ICON, MENU_URL, MENU_DESC) values ('MENU_010_110', 'MENU_010_110_030', '删除操作员', '', 3, 0, 2, '', '/${zhiqim_manager}/operatorDelete.htm', '');
insert into ZMR_MENU (PARENT_CODE, MENU_CODE, MENU_NAME, MENU_NAME_ABBR, MENU_LEVEL, MENU_STATUS, MENU_TYPE, MENU_ICON, MENU_URL, MENU_DESC) values ('MENU_010_110', 'MENU_010_110_040', '操作员权限', '', 3, 0, 2, '', '/${zhiqim_manager}/operatorRuleView.htm', '');
insert into ZMR_MENU (PARENT_CODE, MENU_CODE, MENU_NAME, MENU_NAME_ABBR, MENU_LEVEL, MENU_STATUS, MENU_TYPE, MENU_ICON, MENU_URL, MENU_DESC) values ('MENU_010_110', 'MENU_010_110_060', '设置操作员部门', '', 3, 0, 2, '', '/${zhiqim_manager}/operatorDept.htm', '');
insert into ZMR_MENU (PARENT_CODE, MENU_CODE, MENU_NAME, MENU_NAME_ABBR, MENU_LEVEL, MENU_STATUS, MENU_TYPE, MENU_ICON, MENU_URL, MENU_DESC) values ('MENU_010_110', 'MENU_010_110_070', '设置操作员角色', '', 3, 0, 2, '', '/${zhiqim_manager}/operatorRole.htm', '');
insert into ZMR_MENU (PARENT_CODE, MENU_CODE, MENU_NAME, MENU_NAME_ABBR, MENU_LEVEL, MENU_STATUS, MENU_TYPE, MENU_ICON, MENU_URL, MENU_DESC) values ('MENU_010_110', 'MENU_010_110_080', '操作员参数管理', '', 3, 0, 2, '', '/${zhiqim_manager}/operatorParam.htm', '');
insert into ZMR_MENU (PARENT_CODE, MENU_CODE, MENU_NAME, MENU_NAME_ABBR, MENU_LEVEL, MENU_STATUS, MENU_TYPE, MENU_ICON, MENU_URL, MENU_DESC) values ('MENU_010_110', 'MENU_010_110_200', '角色管理', '', 3, 0, 2, '', '/${zhiqim_manager}/role.htm', '');
insert into ZMR_MENU (PARENT_CODE, MENU_CODE, MENU_NAME, MENU_NAME_ABBR, MENU_LEVEL, MENU_STATUS, MENU_TYPE, MENU_ICON, MENU_URL, MENU_DESC) values ('MENU_010_110', 'MENU_010_110_210', '增加角色', '', 3, 0, 2, '', '/${zhiqim_manager}/roleAdd.htm,/${zhiqim_manager}/roleInsert.htm', '');
insert into ZMR_MENU (PARENT_CODE, MENU_CODE, MENU_NAME, MENU_NAME_ABBR, MENU_LEVEL, MENU_STATUS, MENU_TYPE, MENU_ICON, MENU_URL, MENU_DESC) values ('MENU_010_110', 'MENU_010_110_220', '修改角色', '', 3, 0, 2, '', '/${zhiqim_manager}/roleModify.htm,/${zhiqim_manager}/roleUpdate.htm', '');
insert into ZMR_MENU (PARENT_CODE, MENU_CODE, MENU_NAME, MENU_NAME_ABBR, MENU_LEVEL, MENU_STATUS, MENU_TYPE, MENU_ICON, MENU_URL, MENU_DESC) values ('MENU_010_110', 'MENU_010_110_230', '删除角色', '', 3, 0, 2, '', '/${zhiqim_manager}/roleDelete.htm', '');
insert into ZMR_MENU (PARENT_CODE, MENU_CODE, MENU_NAME, MENU_NAME_ABBR, MENU_LEVEL, MENU_STATUS, MENU_TYPE, MENU_ICON, MENU_URL, MENU_DESC) values ('MENU_010_110', 'MENU_010_110_240', '角色成员', '', 3, 0, 2, '', '/${zhiqim_manager}/roleOperator.htm', '');
insert into ZMR_MENU (PARENT_CODE, MENU_CODE, MENU_NAME, MENU_NAME_ABBR, MENU_LEVEL, MENU_STATUS, MENU_TYPE, MENU_ICON, MENU_URL, MENU_DESC) values ('MENU_010_110', 'MENU_010_110_250', '角色权限', '', 3, 0, 2, '', '/${zhiqim_manager}/roleRule.htm', '');
insert into ZMR_MENU (PARENT_CODE, MENU_CODE, MENU_NAME, MENU_NAME_ABBR, MENU_LEVEL, MENU_STATUS, MENU_TYPE, MENU_ICON, MENU_URL, MENU_DESC) values ('MENU_010_110', 'MENU_010_110_300', '部门管理', '', 3, 0, 2, '', '/${zhiqim_manager}/dept.htm', '');
insert into ZMR_MENU (PARENT_CODE, MENU_CODE, MENU_NAME, MENU_NAME_ABBR, MENU_LEVEL, MENU_STATUS, MENU_TYPE, MENU_ICON, MENU_URL, MENU_DESC) values ('MENU_010_110', 'MENU_010_110_310', '增加部门', '', 3, 0, 2, '', '/${zhiqim_manager}/deptAdd.htm,/${zhiqim_manager}/deptInsert.htm', '');
insert into ZMR_MENU (PARENT_CODE, MENU_CODE, MENU_NAME, MENU_NAME_ABBR, MENU_LEVEL, MENU_STATUS, MENU_TYPE, MENU_ICON, MENU_URL, MENU_DESC) values ('MENU_010_110', 'MENU_010_110_320', '修改部门', '', 3, 0, 2, '', '/${zhiqim_manager}/deptModify.htm,/${zhiqim_manager}/deptUpdate.htm', '');
insert into ZMR_MENU (PARENT_CODE, MENU_CODE, MENU_NAME, MENU_NAME_ABBR, MENU_LEVEL, MENU_STATUS, MENU_TYPE, MENU_ICON, MENU_URL, MENU_DESC) values ('MENU_010_110', 'MENU_010_110_330', '删除部门', '', 3, 0, 2, '', '/${zhiqim_manager}/deptDelete.htm', '');
insert into ZMR_MENU (PARENT_CODE, MENU_CODE, MENU_NAME, MENU_NAME_ABBR, MENU_LEVEL, MENU_STATUS, MENU_TYPE, MENU_ICON, MENU_URL, MENU_DESC) values ('MENU_010_110', 'MENU_010_110_340', '部门成员', '', 3, 0, 2, '', '/${zhiqim_manager}/deptOperator.htm', '');
insert into ZMR_MENU (PARENT_CODE, MENU_CODE, MENU_NAME, MENU_NAME_ABBR, MENU_LEVEL, MENU_STATUS, MENU_TYPE, MENU_ICON, MENU_URL, MENU_DESC) values ('MENU_010_110', 'MENU_010_110_350', '部门权限', '', 3, 0, 2, '', '/${zhiqim_manager}/deptRule.htm', '');
insert into ZMR_MENU (PARENT_CODE, MENU_CODE, MENU_NAME, MENU_NAME_ABBR, MENU_LEVEL, MENU_STATUS, MENU_TYPE, MENU_ICON, MENU_URL, MENU_DESC) values ('MENU_010', 'MENU_010_150', '操作日志查询', '', 2, 0, 1, 'z-text', '/${zhiqim_manager}/operateLog.htm', '');
insert into ZMR_MENU (PARENT_CODE, MENU_CODE, MENU_NAME, MENU_NAME_ABBR, MENU_LEVEL, MENU_STATUS, MENU_TYPE, MENU_ICON, MENU_URL, MENU_DESC) values ('MENU_010_150', 'MENU_010_150_100', '操作员在线列表', '', 3, 0, 2, '', '/${zhiqim_manager}/operatorOnline.htm', '');
insert into ZMR_MENU (PARENT_CODE, MENU_CODE, MENU_NAME, MENU_NAME_ABBR, MENU_LEVEL, MENU_STATUS, MENU_TYPE, MENU_ICON, MENU_URL, MENU_DESC) values ('MENU_010', 'MENU_010_210', '项目信息管理', '', 2, 0, 1, 'z-list', '/gitsolo/project.htm', '');
insert into ZMR_MENU (PARENT_CODE, MENU_CODE, MENU_NAME, MENU_NAME_ABBR, MENU_LEVEL, MENU_STATUS, MENU_TYPE, MENU_ICON, MENU_URL, MENU_DESC) values ('MENU_010', 'MENU_010_220', '项目成员管理', '', 2, 0, 1, 'z-customer', '/gitsolo/member.htm', '');
insert into ZMR_MENU (PARENT_CODE, MENU_CODE, MENU_NAME, MENU_NAME_ABBR, MENU_LEVEL, MENU_STATUS, MENU_TYPE, MENU_ICON, MENU_URL, MENU_DESC) values ('MENU_010_210', 'MENU_010_210_010', '增加项目', '', 3, 0, 2, '', '/gitsolo/projectAdd.htm,/gitsolo/projectInsert.htm', '');
insert into ZMR_MENU (PARENT_CODE, MENU_CODE, MENU_NAME, MENU_NAME_ABBR, MENU_LEVEL, MENU_STATUS, MENU_TYPE, MENU_ICON, MENU_URL, MENU_DESC) values ('MENU_010_210', 'MENU_010_210_020', '修改项目', '', 3, 0, 2, '', '/gitsolo/projectModify.htm,/gitsolo/projectUpdate.htm', '');
insert into ZMR_MENU (PARENT_CODE, MENU_CODE, MENU_NAME, MENU_NAME_ABBR, MENU_LEVEL, MENU_STATUS, MENU_TYPE, MENU_ICON, MENU_URL, MENU_DESC) values ('MENU_010_210', 'MENU_010_210_030', '删除项目', '', 3, 0, 2, '', '/gitsolo/projectDelete.htm', '');
insert into ZMR_MENU (PARENT_CODE, MENU_CODE, MENU_NAME, MENU_NAME_ABBR, MENU_LEVEL, MENU_STATUS, MENU_TYPE, MENU_ICON, MENU_URL, MENU_DESC) values ('MENU_010_210', 'MENU_010_210_040', '转让项目', '', 3, 0, 2, '', '/gitsolo/projectTransferSelector.htm', '');
insert into ZMR_MENU (PARENT_CODE, MENU_CODE, MENU_NAME, MENU_NAME_ABBR, MENU_LEVEL, MENU_STATUS, MENU_TYPE, MENU_ICON, MENU_URL, MENU_DESC) values ('MENU_010', 'MENU_010_230', '项目代码仓库', '', 2, 0, 1, 'z-briefcase', '/gitsolo/repository.htm', '');
insert into ZMR_MENU (PARENT_CODE, MENU_CODE, MENU_NAME, MENU_NAME_ABBR, MENU_LEVEL, MENU_STATUS, MENU_TYPE, MENU_ICON, MENU_URL, MENU_DESC) values ('MENU_010', 'MENU_010_320', '我的项目动态', '', 2, 0, 1, 'z-refresh', '/gitsolo/myReport.htm', '');
commit;

-- ---------------------------------------------------
-- 知启蒙代码仓库独立版功能菜单创建完成
-- ---------------------------------------------------
